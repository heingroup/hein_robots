from unittest import TestCase
from tests.robotics_test_case import RoboticsTestCase
from hein_robots.robotics import Location, Cartesian
from hein_robots.grids import StaticGrid, Grid


class TestGrid(RoboticsTestCase):
    def test_static_grid(self):
        location_grid = {
            'A1': Location(0, 0), 'B1': Location(1, 0), 'C1': Location(2, 0),
            'A2': Location(0, 1), 'B2': Location(1, 1), 'C2': Location(2, 1),
        }

        grid = StaticGrid(list(location_grid.values()), rows=2, columns=3)

        for index, location in grid.locations.items():
            self.assertLocationEqual(location_grid[index], location)
            self.assertLocationEqual(location_grid[index], grid[index])
            self.assertLocationEqual(location_grid[index], grid.locations[index])

    def test_grid_rotation(self):
        # grid = Grid(Location(x=-112, y=138, z=826, rx=89.8, ry=-174.4, rz=177.1), rows=8, columns=12, spacing=Cartesian(9, -10))
        grid = Grid(Location(x=-112, y=138, z=826), rows=8, columns=12, spacing=Cartesian(9, -10))
        print(grid)