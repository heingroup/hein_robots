from typing import Optional
import time
from unittest import TestCase
from hein_robots.robotiq.gripper import RobotiqGripper


class GripperTest(TestCase):
    gripper: Optional[RobotiqGripper] = None

    @classmethod
    def setUpClass(cls) -> None:
        cls.gripper = RobotiqGripper('192.168.254.88')

    @classmethod
    def tearDownClass(cls) -> None:
        cls.gripper.disconnect()

    def test_connection(self):
        self.assertIsNotNone(self.gripper)

    def test_read(self):
        self.assertIsNotNone(self.gripper.position)
        self.assertIsNotNone(self.gripper.status)

    def test_activate(self):
        self.gripper.activate()
        self.assertTrue(self.gripper.active)

    def test_move(self):
        self.gripper.move(0.5)
        self.assertAlmostEqual(self.gripper.position, 0.5, places=2)
        self.assertEqual(self.gripper.position_int, 128)

        self.gripper.move(0.75)
        self.assertAlmostEqual(self.gripper.position, 0.75, places=2)

        self.gripper.move(0.9)
        self.assertAlmostEqual(self.gripper.position, 0.9, places=2)

        self.gripper.move(0.1)
        self.assertAlmostEqual(self.gripper.position, 0.1, places=2)

    def test_open(self):
        self.gripper.open()
        self.assertAlmostEqual(self.gripper.position, 0.0, places=1)

    def test_close(self):
        self.gripper.close()
        self.assertAlmostEqual(self.gripper.position, 1.0, places=1)

    def test_grab(self):
        self.gripper.close(force=0.005)
        self.gripper.open()