from unittest import TestCase
import math
import time
from hein_robots.robotics import Location
from hein_robots.universal_robots.ur3 import UR3Arm


class UR3ArmTest(TestCase):
    start_joints = [-91.71336593043071, -98.95555211117863, -126.22259618359665, -46.29498485567708, 91.39250049132535, -1.776167015370807]
    start_location = Location(x=-118.4, y=-268.0, z=157.3, rx=-180, ry=0, rz=180.0)

    @classmethod
    def setUpClass(cls) -> None:
        cls.arm = UR3Arm('ursim')

    @classmethod
    def tearDownClass(cls) -> None:
        cls.arm.close()

    def assertLocationEqual(self, location_a: Location, location_b: Location, places=1):
        self.assertAlmostEqual(location_a.x, location_b.x, places=places)
        self.assertAlmostEqual(location_a.y, location_b.y, places=places)
        self.assertAlmostEqual(location_a.z, location_b.z, places=places)
        self.assertAlmostEqual(location_a.rx, location_b.rx, places=places)
        self.assertAlmostEqual(location_a.ry, location_b.ry, places=places)
        self.assertAlmostEqual(location_a.rz, location_b.rz, places=places)

    def test_connect(self):
        self.assertIsNotNone(self.arm)
        print(self.arm)

    def test_location(self):
        pose = self.arm.robot.get_pose()
        angles = list(map(lambda i: math.degrees(i), pose.orient.to_euler('xyz')))
        location = self.arm.location
        self.assertAlmostEqual(pose.pos[0], location.x)
        self.assertAlmostEqual(pose.pos[1], location.y)
        self.assertAlmostEqual(pose.pos[2], location.z)
        self.assertAlmostEqual(angles[0], location.rx)
        self.assertAlmostEqual(angles[1], location.ry)
        self.assertAlmostEqual(angles[2], location.rz)

    def test_move(self):
        targets = [
            Location(-.1, -.322, .449, -80, 31, -140),
            Location(-.1, -.322, .439, -80, 31, -140),
        ]

        for target in targets:
            self.arm.move_to_location(target)
            self.assertLocationEqual(self.arm.location, target)

    def test_move_joints(self):
        targets = [
            ([-97.35, -108.65, -79.33, 19.45, 131.58, 39.06], Location(-.1, -.322, .449, -80, 31, -140)),
            ([-97.35, -109.03, -81.52, 22.03, 131.58, 19.06], Location(-.1, -.322, .439, -80, 30.99, -140)),
        ]

        for joints, location in targets:
            self.arm.move_joints(joints)
            self.assertLocationEqual(self.arm.location, location)


    def test_urx_move(self):
        self.arm.robot.movel((-0., -0.300, -0.250, 0, 0, 0))

    def test_tool_space_move(self):
        self.arm.move_joints(self.start_joints)
        self.arm.move_to_location(self.start_location)
        self.arm.move_tool_to_location(Location(x=100))
        self.assertLocationEqual(self.arm.location, self.start_location.copy(x=-218.4, rx=180))

        self.arm.move_joints(self.start_joints)
        self.arm.move_to_location(self.start_location)
        self.arm.move_tool(x=100)
        self.assertLocationEqual(self.arm.location, self.start_location.copy(x=-218.4, rx=180))

        self.arm.move_joints(self.start_joints)
        self.arm.move_to_location(self.start_location)
        self.arm.move_tool(y=100)
        self.assertLocationEqual(self.arm.location, self.start_location.copy(y=-168, rx=180))

    def test_circular_move(self):
        self.arm.move_joints(self.start_joints)
        self.arm.move_to_location(self.start_location)

        radius = 100

        midpoint = self.start_location.translate(y=math.cos(math.pi/2) * radius, z=math.sin(math.pi/2) * radius)
        end = self.start_location.translate(y=math.cos(math.pi) * radius, z=math.sin(math.pi) * radius)

        self.arm.move_circular(midpoint, end)
        self.arm.move_circular(midpoint, self.start_location)
        print('done')


