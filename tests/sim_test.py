from hein_robots.robodk.sim_arm import SimArm
from hein_robots.robotics import np, Rotation, Orientation, Location, Frame
from PySide2.QtGui import QQuaternion, QTransform, QMatrix3x3, QMatrix4x4
from robodk import Pose_2_Fanuc, Fanuc_2_Pose, Pose_2_TxyzRxyz, TxyzRxyz_2_Pose, transl, rotx, roty, rotz


arm = SimArm()
# arm.home()
arm.move(x=10, relative=True)
arm.move(y=-100, relative=True)
print(arm)