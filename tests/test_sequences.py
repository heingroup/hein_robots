from hein_robots.robotics import Location
from hein_robots.kinova.kinova_gen3 import KinovaGen3Arm
from hein_robots.universal_robots.urscript_sequence import URScriptSequence
from hein_robots.kinova.kinova_sequence import KinovaSequence, KortexSequence
from hein_robots.utils import HeinRobotsTestCase


class UR3ArmTest(HeinRobotsTestCase):
    def test_kortex_sequence(self):
        locations = ['park', 'capped', 'uncapped', 'alternative_park']
        joints = ['Demo pose 1', 'Demo pose 2']

        arm = KinovaGen3Arm()

        locations_sequence = KinovaSequence('./data/kinova_cap_sequence.json')
        cap_sequence = KortexSequence(arm, 'cap')

        self.assertEqual(len(locations_sequence.locations), len(cap_sequence.locations))

        for location in locations:
            self.assertLocationEqual(locations_sequence[location], cap_sequence[location])

        joints_sequence = KinovaSequence('./data/kinova_demo_sequence.json')
        demo_sequence = KortexSequence(arm, 'Demo Sequence')

        # the kortex sequence doesn't include the 2 extra linked actions from "Demo Sequence"
        self.assertEqual(len(demo_sequence.joints), len(joints))

        for joint in joints:
            self.assertListEqual(joints_sequence.joints[joint], demo_sequence.joints[joint])

    def test_ur_sequence(self):
        locations = {
            'approach_wall': Location(-0.24532573277, 0.229897092991, 0.281144807052, 86.02179904982493, 0.16713727820230836, -91.23572472469071),
            'Waypoint_7': Location(-0.2854605744, 0.226833034366, 0.284665759716, 85.20365081679955, -0.7291011809048108, -91.5031664812526),
            'Waypoint_8': Location(-0.177989037085, 0.23215907321, 0.27794987569, 86.87261642351537, 2.6030178645436783, -93.55101837600304),
            'turnup': Location(-0.180349575671, 0.157332370935, 0.493822704667, 87.8440714833273, -1.0845824746582937, -96.5402466975863),
            'Waypoint_4': Location(-0.194118309591, 0.004480212183, 0.418249273896, 93.20919754984095, -6.381739483259029, -58.84715423409578),
            'Waypoint_5': Location(-0.010432654272, -0.137842686751, 0.418113086049, 86.05325814903043, -2.1699327289842305, 37.76080632931398),
            'Waypoint_6': Location(-0.365555132608, -0.054481280937, 0.393017969005, 90.14754546411154, -4.152103397312745, -87.08871153882232),
            'swing': Location(-0.238932901566, -0.204242345326, 0.344937874978, 89.12748802263647, -2.524056100383436, -93.37563218503982),
            'Waypoint_3': Location(-0.088096149861, -0.258962403149, 0.268999243134, 88.48865611592076, 0.794972283975372, -89.44323631745216),
            'Waypoint_1': Location(-0.111737291356, -0.40671506337, 0.305580168686, 89.13951594802595, 1.0619926547539347, -85.75134903297479),
            'Waypoint_2': Location(-0.150081342622, -0.413660750094, 0.30080669051, 87.25291274666989, 1.3005313958152807, -87.09246330251099),
        }

        joints = {
            'approach_wall': [-4.0740631262408655, -1.4224770826152344, -1.927189826965332, 3.463268442744873, -2.476745907460348, 12.65368092853764],
            'Waypoint_7': [-3.8922274748431605, -1.4223812383464356, -1.9268178939819336, 3.4610163408466796, -2.2889912764178675, 12.653729089090618],
            'Waypoint_8': [-4.380915466939108, -1.4224413198283692, -1.926866054534912, 3.4927031236835937, -2.744894329701559, 12.653692849466594],
            'turnup': [-3.9880502859698694, -1.3265610498240967, -0.7969446182250977, 2.1724211412617187, -2.3021231333362024, 12.616988786051067],
            'Waypoint_4': [-3.3298402468310755, -1.0266400140574952, -1.2931017875671387, 2.2441012102314453, -2.3080604712115687, 12.627032883951458],
            'Waypoint_5': [-1.2293694655047815, -0.6160436433604737, -1.3877429962158203, 2.07587115346875, -1.8857019583331507, 12.626756556818279],
            'Waypoint_6': [-1.229034725819723, -0.8518124383739014, -1.5524563789367676, 2.416686936015747, 0.290438175201416, 12.626744635889324],
            'swing': [-0.9178507963763636, -1.4331792157939454, -1.5528168678283691, 2.9639193254658203, 0.7133505344390869, 12.626744635889324],
            'Waypoint_3': [-0.5783303419696253, -1.828940053979391, -1.500065803527832, 3.2977968889423828, 0.9837746620178223, 12.569685586283],
            'Waypoint_1': [-0.9228084723102015, -2.633015295068258, 0.07082158723940069, 2.5313541132160644, 0.571479320526123, 12.575061448404583],
            'Waypoint_2': [-1.0220826307879847, -2.6350490055479945, 0.002930943165914357, 2.5280777651020507, 0.49686217308044434, 12.636223920175823],
        }

        sequence = URScriptSequence('./data/sequence.urscript')

        for name, location in locations.items():
            self.assertIn(name, sequence.locations)
            self.assertLocationEqual(location, sequence.locations[name])

            self.assertIn(name, sequence.joints)
            self.assertListAlmostEqual(joints[name], sequence.joints[name])

    def test_kinova_sequence(self):
        locations = {
            'v_engage': Location(x=-225.0, y=-279.2, z=954.3, rx=-0.08152, ry=0.7054, rz=99.01),
            'v-pose': Location(x=-225.1, y=-279.2, z=723.3, rx=-0.09079, ry=0.6969, rz=99.0)
        }

        joints = {
            'home': [250.83840942382812, 252.2596435546875, 210.5957489013672, 241.89564514160156, 76.47003173828125, 30.59441375732422, 44.22319412231445],
            'v': [331.76361083984375, 252.2379150390625, 210.50466918945312, 241.98509216308594, 76.77520751953125, 30.890762329101562, 43.95873260498047],
            'h': [279.826171875, 289.0525207519531, 200.81825256347656, 264.92938232421875, 83.33824920654297, 116.02796936035156, 111.49574279785156]
        }

        sequence = KinovaSequence('./data/kinova_sequence.json')

        for name, location in locations.items():
            self.assertIn(name, sequence.locations)
            self.assertLocationEqual(location, sequence[name])

        for name, joints in joints.items():
            self.assertIn(name, sequence.joints)
            self.assertListAlmostEqual(joints, sequence.joints[name])
