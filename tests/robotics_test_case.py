from typing import List, Any
from unittest import TestCase
from hein_robots.robotics import Location


class RoboticsTestCase(TestCase):
    def assertAngleAlmostEqual(self, angle_a: float, angle_b: float, places=4):
        rounded_angle_a = round(angle_a, places)
        rounded_angle_b = round(angle_b, places)

        if rounded_angle_a == rounded_angle_b:
            return

        if rounded_angle_a + 360 == rounded_angle_b:
            return

        if rounded_angle_a == rounded_angle_b + 360:
            return

        raise AssertionError(f'Angles not equal: {angle_a} != {angle_b} (+/- 360.0)')

    def assertAnglesAlmostEqual(self, list_a: List[Any], list_b: List[Any], places=2):
        self.assertEqual(len(list_a), len(list_b))

        for index in range(len(list_a)):
            self.assertAngleAlmostEqual(list_a[index], list_b[index], places=places)

    def assertLocationEqual(self, location_a: Location, location_b: Location, places=1):
        self.assertAlmostEqual(location_a.x, location_b.x, places=places)
        self.assertAlmostEqual(location_a.y, location_b.y, places=places)
        self.assertAlmostEqual(location_a.z, location_b.z, places=places)
        self.assertAlmostEqual(location_a.rx, location_b.rx, places=places)
        self.assertAlmostEqual(location_a.ry, location_b.ry, places=places)
        self.assertAlmostEqual(location_a.rz, location_b.rz, places=places)
