from hein_robots.kinova.kinova_gen3 import KinovaGen3Arm

arm = KinovaGen3Arm()
print('velocity: ', arm.velocity)
print('acceleration: ', arm.acceleration)
print('twist: ', arm.twist)
print('wrench: ', arm.wrench)
print('joint count:', arm.joint_count)
print('joint positions: ', arm.joint_positions)
print(arm)