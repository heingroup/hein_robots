from hein_robots.robotics import np, Rotation, Units, Vector, Cartesian, Orientation, Frame, Location, Twist, Wrench
from hein_robots.grids import Grid
from hein_robots.base.robot_arms import RobotArm
from hein_robots.kinova.kinova_gen3 import KinovaGen3Arm

ls = np.array([
    [Location(0, 0, 0), Location(1, 0, 0), Location(2, 0, 0)],
    [Location(0, 1, 0), Location(1, 1, 0), Location(2, 1, 0)],
])
location = Location(1, 1, 1, rz=90)
scale = Cartesian(2.5, 1.2, 1)
rotation = Orientation(rz=-90)
grid = Grid(Location(1, 1, 0, rz=90), rows=2, columns=3, spacing=(1.5, 2.5))
print(grid.location_grid)
print(Grid.from_locations(Location(-4, 1, 0), Location(), Location(0, 4, 0), rows=2, columns=2).location_grid)
# print(Grid.from_locations(Location(-4, 1, 2), Location(), Location(0, 4, 0), rows=2, columns=2).location_grid)
# print(location * ls)
print('ready')