import time
from hein_robots.kinova.kinova_gen3 import KinovaGen3Arm

arm = KinovaGen3Arm()

arm.home()
arm.move(z=-10, relative=True)
arm.move(z=20, relative=True)
arm.move(x=-20, relative=True)

print('done')