from tests.robotics_test_case import RoboticsTestCase
from hein_robots.robotics import Location
from hein_robots.kinova.kinova_gen3 import KinovaGen3Arm


class TestKinova(RoboticsTestCase):
    arm: KinovaGen3Arm = None

    @classmethod
    def setUpClass(cls) -> None:
        cls.arm = KinovaGen3Arm()

    def test_forward_kinematics(self):
        joints = self.arm.joint_positions
        location = self.arm.forward_kinematics(joints)
        self.assertLocationEqual(self.arm.location, location)

    def test_inverse_kinematics(self):
        location = self.arm.location
        joints = self.arm.inverse_kinematics(location)
        self.assertAnglesAlmostEqual(joints, self.arm.joint_positions, places=0)