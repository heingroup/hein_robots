import cv2
from hein_robots.vision import Camera, MarkerDetector, AprilTagsMarkerDetector
from di_registry import registry

registry.register(Camera, 'camera')
registry.register(AprilTagsMarkerDetector, 'detector')
# registry.register(ArucoMarkerDetector, 'detector')
registry.configure('./files/marker_detection_config.yaml')

camera: Camera = registry.get('camera')
detector: MarkerDetector = registry.get('detector', camera=camera)


while True:
    image = camera.get_image()
    markers = detector.detect(image)

    if len(markers) > 0:
        print()
        for marker in markers:
            print(f'Marker ID: {marker.id}, loc: {marker.location}, cam: {marker.location_camera}')

    cv2.imshow('camera', image)

    if cv2.waitKey(1) & 0xff == ord('q'):
        break

cv2.destroyAllWindows()