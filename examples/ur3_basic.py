from hein_robots.universal_robots.ur3 import UR3Arm
from hein_robots.universal_robots.urscript_sequence import URScriptSequence

# import your sequence(s) from a URScript file
sequence = URScriptSequence('./files/sequence.urscript')

# create an instance o UR3Arm
arm = UR3Arm()  # the first argument is an optional hostname / ip for the arm, defaults to 192.168.0.100

# move the arm to a joint configuration (movep)
arm.move_joints(sequence.joints['Waypoint_1'])

# perform a linear move to a location
arm.move_to_location(sequence['Waypoint_2'])


from hein_robots.robotics import Location

# move 10mm upwards
arm.move(z=10, relative=True)
#  or the equivalent move with Location math:
arm.move_to_location(arm.location + Location(z=10))  # arm.location returns a Location
# rotate the arm 90 degrees around the y axis
arm.move(rz=90, relative=True)
#  or the equivalent with Location math:
arm.move_to_location(arm.location + Location(rz=90))

# locations contain a position and an orientation
location = arm.location.copy()  # locations can be copied
print(location.position)
print(location.orientation)
location_b = location.copy(z=200)  # you can update any values in the location copy easily